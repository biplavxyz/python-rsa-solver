#Can be used in ctf challenges with given n, e, and c.
#This code only solves rsa challenges with small values, and is not meant to solve complex rsa challenges.
print("================================================")
print("RSA Decryption Calculator")
print("Coded by seizetheday")
print("================================================")

#Taking Input; modulus(m), encryption exponent(e), and ciphertext(c)
print("Enter the value of n: ")
n = int(input())
print("================================================")

print("Enter the value of e: ")
e = int(input())
print("================================================")

print("Enter ciphertext: ")
c = input()
cipher = c.split(" ")
print("================================================")


#Backup initial value for n and also define a new list
n_bak = n
primes = []

#Calculate two prime factors p and q from given n
for i in range(2, n+1):
    while n % i == 0:
        primes.append(i)
        n = n/i 

#Prints the prime values
print(f"The value of two primes are {primes}")
print("================================================")


#Calculate phi from those primes
phi_n = ((primes[0])-1) * ((primes[1])-1)
print(f"The value of phi_n is {phi_n}")
print("================================================")

#Calculate the secret exponent or decryption exponent
for i in range(0,e+1):
    if (1 + (i * phi_n)) % e == 0:
        print(f"D is calculated by substituting i with {i}.")
        global d 
        d = int((1 + (i * phi_n)) / e)
        print(f"The value of d is: {d}.")

print("================================================")

#Decrypt ciphertext and print plaintext ascii
print("Flag is: ")
for i in cipher:
    test = (int(i)**d) % n_bak
    print(chr(test), end = "")
print()
print("================================================")
